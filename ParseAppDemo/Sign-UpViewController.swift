//
//  Sign-UpViewController.swift
//  ParseAppDemo
//
//  Created by Rose on 22/03/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit

class Sign_UpViewController: UIViewController {

    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBAction func signUpBurronAction(_ sender: Any) {
        
        let classObject = PFObject(className: "DataClass")
        
        classObject["username"] = userNameTextField.text
        classObject["email"] = EmailTextField.text
        classObject["password"] = PasswordTextField.text
        
        classObject.saveInBackground { (successfull, error) in
            if successfull {
                do{
                try classObject.save()
                }catch{
                    
                }
            }else{
                
            }
           self.showAlert(title: "Success", message: "Insert Success")
        }
    }
    
    @IBAction func DissmissViewControllerActionButton(_ sender: UIButton) {
        let signInControl = self.storyboard?.instantiateViewController(withIdentifier: "main")as! ViewController
        self.navigationController?.pushViewController(signInControl, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: title, style: UIAlertActionStyle.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
