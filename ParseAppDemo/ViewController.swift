//
//  ViewController.swift
//  ParseAppDemo
//
//  Created by Rose on 21/03/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBAction func LoginButtonAction(_ sender: UIButton) {
        
        let query = PFQuery(className: "DataClass")
        query.whereKey("email", equalTo: EmailTextField.text!)
        query.whereKey("password", equalTo: PasswordTextField.text!)
        
        query.findObjectsInBackground { (results, error) in
            if (results?.count)! > 0{
                
//                resultStore.set(results, forKey: "logindata")
                    let homePage = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as! HomeViewController
                    
                    self.navigationController?.pushViewController(homePage, animated: true)
                }
                else{
                    print("Data Not Match ");
                }
        }
    }
    
    @IBAction func RegitrationButtonAction(_ sender: UIButton) {
        let signUpControl = self.storyboard?.instantiateViewController(withIdentifier: "Sign_UpViewController")as! Sign_UpViewController
        self.navigationController?.pushViewController(signUpControl, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

